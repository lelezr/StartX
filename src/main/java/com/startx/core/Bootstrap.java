package com.startx.core;

import com.startx.core.accesspoint.AccessPointStarter;
import com.startx.core.config.ConfigStarter;
import com.startx.core.netty.server.Server;

/**
 * 服务启动入口
 */
public class Bootstrap {
	
	/**
	 * 启动服务
	 */
	public static void start() throws Exception {
		// 初始化配置文件
		ConfigStarter.start();
		// 初始化AccessPoint，初始化Spring
		AccessPointStarter.start();
		// 启动NettyServer
		Server.start();
	}
}
