package com.startx.core.config;

import java.io.IOException;

import com.startx.core.config.reader.impl.PropertiesConfigReader;

/**
 * 启动配置解析
 */
public class ConfigStarter {
	
	/**
	 * 启动配置解析
	 * @throws Exception 
	 * @throws IOException 
	 */
	public static void start() throws IOException, Exception {
		new PropertiesConfigReader().read();
	}
	
}
