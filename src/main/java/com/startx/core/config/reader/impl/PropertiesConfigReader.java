package com.startx.core.config.reader.impl;

import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import com.startx.core.config.holder.ConfigHolder;
import com.startx.core.config.reader.ConfigReader;
import com.startx.core.netty.server.impl.HttpServer;

/**
 * 配置读取接口
 */
public class PropertiesConfigReader implements ConfigReader {
	
	private static final Logger Log = Logger.getLogger(HttpServer.class);
	
	/**
	 * 读取配置
	 * @throws Exception 
	 */
	@Override
	public void read() throws Exception {
		
		Properties prop = new Properties();
		prop.load(PropertiesConfigReader.class.getResourceAsStream("/config.properties"));
		
		Set<Object> keySet = prop.keySet();
		
		for (Object key : keySet) {
			
			String name  = key.toString();
			String value = prop.get(key).toString();
			
			ConfigHolder.setConfig(name, value);
		}
		
		Log.info(ConfigHolder.getConfig());
	}
	
}
