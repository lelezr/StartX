package com.startx.core.accesspoint.factory;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.startx.core.system.model.AccessPointTarget;

/**
 * 接入点工厂
 */
public class AccessPointFactory {
	
	/**
	 * log 工具
	 */
	private static final Logger Log = Logger.getLogger(AccessPointFactory.class);
	/**
	 * 添加接入点
	 */
	private static final Map<String,AccessPointTarget> ACCESS_POINT = new HashMap<String, AccessPointTarget>();
	
	public static void setAccessPoint(String name,AccessPointTarget value) {
		
		if(ACCESS_POINT.containsKey(name)) {
			throw new RuntimeException("AccessPoint定义重复，请检查："+name);
		}
		
		Log.info(name+"_"+value.getType().name());
		
		ACCESS_POINT.put(name, value);
	}
	
	/**
	 * 获取接入点
	 * @param name
	 * @return
	 */
	public static AccessPointTarget getAccessPoint(String name) {
		return ACCESS_POINT.get(name);
	}
}
