package com.startx.action;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;

import com.startx.core.accesspoint.anotation.AccessPoint;
import com.startx.core.accesspoint.anotation.RequestMethod;
import com.startx.core.accesspoint.anotation.RequestPoint;
import com.startx.core.accesspoint.anotation.ResponseType;
import com.startx.core.netty.output.http.JsonOutput;
import com.startx.core.system.Colorfulogo;
import com.startx.core.system.param.HttpArgs;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponseStatus;

@Controller
@AccessPoint("/login")
public class LoginAction {
	
	@RequestPoint(value={"/empty/param"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void emptyParam() {
		
		System.out.println("login...");
		
	}
	
	@RequestPoint(value={"/body/ctx/args"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void test02(byte[] body,ChannelHandlerContext ctx,HttpArgs args) throws UnsupportedEncodingException {
		
		Map<String,Object> response = new HashMap<>();
		response.put("colorfulogo", Colorfulogo.get());
		JsonOutput.object(ctx, HttpResponseStatus.OK,response);
		
	}
	
	@RequestPoint(value={"/args/body"},method=RequestMethod.GET,type=ResponseType.JSON)
	public void test03(HttpArgs args,byte[] body) {
		
		System.out.println("login...");
		
	}
	
	@RequestPoint(value={"/args"},method=RequestMethod.GET,type=ResponseType.JSON)
	public Map<String,Object> test04(HttpArgs args) {
		
		System.out.println("login... ");
		Map<String,Object> response = new HashMap<>();
		response.put("colorfulogo", Colorfulogo.get());
		
		return response;
	}
	
}
